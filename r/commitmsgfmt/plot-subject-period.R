subject_periods <- read.table("subject-period", header = TRUE)
sp <- reshape2::melt(subject_periods, id = c("year", "project"))
ggplot2::ggplot(sp, ggplot2::aes(x = year, y = value, colour = variable)) +
  ggplot2::facet_wrap(~ project, nrow = 2) +
  ggplot2::geom_line(position = "fill", stat = "identity") +
  ggplot2::scale_x_continuous(breaks = seq(2002, 2018, 2)) +
  ggplot2::scale_colour_hue(
    labels = c("Commits per year", "Subject lines ending in '.'")
  ) +
  ggplot2::labs(
    title = "Subject lines ending in '.'",
    x = "Year",
    y = "Fraction of commits"
  ) +
  ggplot2::theme_minimal() +
  ggplot2::theme(
    legend.title = ggplot2::element_blank(),
    legend.position = "bottom",
    panel.grid.minor.x = ggplot2::element_blank()
  )
