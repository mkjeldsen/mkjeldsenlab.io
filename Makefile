RSCRIPT ?= ./Rscript
HUGO ?= ./hugow

STATIC =
STATIC += commitmsgfmt/ecdf.png
STATIC += commitmsgfmt/subject-period.png

STATIC := $(addprefix static/img/, $(STATIC))
STATIC_PREFIX := $(sort $(dir $(STATIC)))

# Execute Rscript in the script-local context, generating a plot with height $1
# and width $2. Names of input and output are automagically inferred from target
# and dependency names using various "automatic variables".
mkplot_h_w = $(RSCRIPT) \
	  -e 'WD <- setwd("$(<D)")' \
	  -e 'source("$(<F)")'\
	  -e 'setwd(WD)' \
	  -e 'ggplot2::ggsave("$@", height = $(1), width = $(2), dpi = 150)' && \
	  optipng $@

.PHONY: all
all: public $(STATIC_PREFIX) $(STATIC)

.PHONY: serve
serve:
	$(HUGO) serve -D

public:
	$(HUGO)

$(STATIC_PREFIX):
	mkdir -p $@

static/img/commitmsgfmt/ecdf.png: r/commitmsgfmt/main.R
	$(call mkplot_h_w,2.0,4.0)

static/img/commitmsgfmt/subject-period.png: r/commitmsgfmt/plot-subject-period.R
	$(call mkplot_h_w,4.0,4.0)
