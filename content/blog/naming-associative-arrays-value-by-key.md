+++
title = "Naming associative arrays: value-by-key"
date = 2023-11-19T15:38:50+01:00
draft = false
tags = [ "naming" ]
+++

I prefer to name instances of [associative arrays][url-aa] according to
a pattern I call [value-by-key](#value-by-key). I picked up the pattern on
reddit long ago, from a comment fully lost to time. I have found this pattern
helpful for comprehension but I rarely encounter in the wild. In this post
I will demonstrate the pattern and contrast it with patterns I encounter more
often.

I will use the terminology from the previously linked Wikipedia article on
associative arrays. I will provide code examples in Java 21, where the
associative array is defined by [`Map<K,V>`][url-aa-jdk][^fn-hashtable], and
Python 3.11, where the associative array is defined by
[_dictionary_][url-aa-python][^fn-dict]. This covers the two casing and naming
patterns I am most familiar with and encounter most frequently; in particular,
it covers both OpenJDK and .NET.

- [Value-by-key](#value-by-key)
- [Alternative patterns](#alternative-patterns)
    - [Key-value-array](#key-value-array)
    - [Key-to-value](#key-to-value)
    - [Key-value](#key-value)

## Value-by-key

In general terms, I name associative arrays in the style of "<var>value</var>
by <var>key</var>". By way of example, here we look up some <var>film</var>
using two different keys[^fn-example]:

{{< highlight java >}}
film = filmByActor.get(actor);
film = filmByExecutiveProducer.get(execProducer);
{{< / highlight >}}

{{< highlight python >}}
film = film_by_actor[actor]
film = film_by_executive_producer.get(exec_producer)
{{< / highlight >}}

The value-by-key pattern has some practical advantages. To understand those
advantages we must first recognize that we should read the code from the
perspective of a consumer, not a producer; both consumer and producer are
callers but they operate in different contexts.

A consumer has an objective that involves utilizing the value. In the problem
the consumer needs to solve, the key used to obtain the value is a secondary
concern. By putting the value in the leading position we emphasize the desired
outcome; avoiding an unnecessary barrier to comprehension and more directly
expressing the objective. By contrast, the producer is primarily concerned with
the act of recording the association between keys and values.

The "by" preposition separates the element names to prevent ambiguity where an
element name is a compound, and the exact meaning of ["by"][url-define-by]
communicates an existing arrangement of values in relation to keys. That second
point makes this pattern [declarative], similar to [the a-of-b pattern used in
OCaml][ocaml].

Arranging different ways of accomplishing the same goal together makes it easy
and convenient to extract signal from noise; to determine whether it is even
possible to do what you want, and if so, what options are available.
Dynamically generated identifier lists are often ordered alphabetically by
default. Static identifier lists can be manually arranged to attain the same
benefits. API documentation, code structure listings, debugger symbol listings,
and of course variable declarations are good examples of such identifier lists.
Assigning shared prefixes to identifiers trivializes grouping by alphabetical
order.

## Alternative patterns

All other consistently applied patterns I have encountered are variations of
having the key in the leading position and the value in the following position.
Inevitably, these variations appear to be essentially the same pattern.
Effectively, they _are_ the same, but there are specific circumstances that
make each pattern more likely than another.

### Key-value-array

The _key-value-array_ pattern mirrors the way we tend to pronounce the type of
an associative array, where transposing the array and element names turns the
element names into an adjective describing the array-noun:

{{< highlight java >}}
film = actorFilmMap.get(actor);
film = executiveProducerFilmMap.get(execProducer);
{{< / highlight >}}

{{< highlight python >}}
film = actor_film_dict[actor]
film = executive_producer_film_dict.get(exec_producer)
{{< / highlight >}}

This pattern explicitly refers to the associative array as a noun, emphasizing
the array itself and downplaying the _association_, when in reality the
defining characteristic of an associative array is the association it
facilitates. Additionally, the pattern allows ambiguity in element names
comprised of compounds: it is not immediately clear which of "executive",
"producer", "film", and "array" bind together.

When JetBrains' _IntelliJ IDEA 2023_ IDE suggests associative array names it
uses the key-value-array pattern. The following picture is from IntelliJ IDEA
Community Edition 2023.1.2:

![IntelliJ IDEA generates key-value-map names](/img/assocarray/idea.png)

None of the four names IntelliJ suggests are objectively good and half are
outright useless, and I know of no IDE that performs better in this respect.

### Key-to-value

Where the key-value-array pattern emphasizes the concrete manifestation of an
association of keys to values, the _key-to-value_ pattern correctly emphasizes
the association itself:

{{< highlight java >}}
film = actorToFilm.get(actor);
film = executiveProducerToFilm.get(execProducer);
{{< / highlight >}}

{{< highlight python >}}
film = actor_to_film[actor]
film = executive_producer_to_film.get(exec_producer)
{{< / highlight >}}

However, in contrast to the value-by-key pattern, which optimizes for answering
the question of how you can get something you want, this pattern optimizes for
answering the question of what you can get with something you already have. To
a consumer, that perspective is generally irrelevant if not outright unhelpful:
there are usually many ways we can turn something we have into something we
don't want, but often only a few ways to get something we want no matter what
we have.

Like the "by" preposition in the value-by-key pattern, the "to" preposition
limits ambiguity where an element name is a compound. However, unlike "by", the
meaning of ["to"][url-define-to] communicates impending translation of a key to
a value, as distinct from an existing arrangement of elements[^fn-telic].
Although this is semantic quibbling it has the consequence that, while both
patterns accurately describe a mathematical function from key to value, the
key-to-value pattern better describes the programming construct that can be
invoked whereas the value-by-key pattern better describes the state of having
previously invoked such a programming construct.

### Key-value

In application code I frequently encounter the key-value-array and key-to-value
patterns. A terser _key-value_ pattern, meanwhile, I mostly encounter in
[relational theory][url-ae]:

{{< highlight java >}}
film = actorFilm.get(actor);
film = executiveProducerFilm.get(execProducer);
{{< / highlight >}}

{{< highlight python >}}
film = actor_film[actor]
film = executive_producer_film.get(exec_producer)
{{< / highlight >}}

Of every pattern discussed here the key-value pattern is the weakest one for
application code. The key-value pattern leaves it unclear which element name is
the key and which is the value, and element names comprised of compounds
exacerbates this ambiguity. In relational theory, on the other hand, this
pattern tends to work out better than every other option discussed precisely
because the association is bidirectional.

[^fn-dict]: The type name of Python's dictionary implementation is `dict`.
[^fn-example]: We disregard that all the relationships between films, actors, and executive producers are many-to-many.
[^fn-hashtable]: JDK 21 also has `Dictionary<K,V>` but this is an ancient class that only exists for compatibility purposes.
[^fn-telic]: Grammatically, "a to b" is _telic_ and "b by a" is _atelic_. However, this property has little practical utility and is likely to confuse more than aid.

[url-aa-jdk]: https://docs.oracle.com/en/java/javase/21/docs/api/java.base/java/util/Map.html "JDK 21 documentation: Map"
[url-aa-python]: https://docs.python.org/3.11/tutorial/datastructures.html#dictionaries "Python 3.11 documentation: dictionaries"
[url-aa]: https://en.wikipedia.org/w/index.php?title=Associative_array&oldid=1182282630 "Wikipedia: Associative array"
[url-ae]: https://en.wikipedia.org/w/index.php?title=Associative_entity&oldid=1182282470 "Wikipedia: Associative entity"
[url-define-by]: https://www.merriam-webster.com/dictionary/by#dictionary-entry-1 "Definition of "by""
[url-define-to]: https://www.merriam-webster.com/dictionary/to#dictionary-entry-1 "Definition of "to""
[declarative]: https://en.wikipedia.org/w/index.php?title=Declarative_programming&oldid=1184514070 "Wikipedia: Declarative programming"
[ocaml]: https://stackoverflow.com/a/59718781/482758 "Stack Overflow: Why is the OCaml type conversion function named in the form "A_of_B"?"
