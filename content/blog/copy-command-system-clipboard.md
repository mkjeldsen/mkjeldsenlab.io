+++
title = "Copy command to system clipboard"
date = 2018-05-30T19:09:13+02:00
draft = false
tags = [ "bash", "ergonomics" ]
+++

In Bash you can copy the previous command to the system clipboard directly from
the command line, and have it be correctly quoted, using

{{< highlight bash >}}
$ echo !!:q | xsel --clipboard
{{< / highlight >}}

This is very useful for sharing or embedding complex commands. Consider this
example, with a command that includes quotation marks:

{{< highlight bash >}}
$ echo 'foo bar'
foo bar
$ echo !!:q | xsel -b
echo 'echo '\''foo bar'\''' | xsel -b
$ xsel -b
echo 'foo bar'
{{< / highlight >}}

The first command, in its entirety, is the command we're interested in copying.
In the second command, the `!!` history expansion _event designator_ selects
the previously executed command, and the `q` _modifier_ wraps the command in
quotation marks, escaping characters as necessary. Wrapping the command in
quotation marks turns it into a single argument to the second `echo`, which
pipes it into `xsel`'s standard input. The final command dumps the contents of
the clipboard _selection_: `echo 'foo bar'`, as desired.

You might have [`xclip(1)`][man-xclip] instead of [`xsel(1)`][man-xsel], in
which case you'll need to pass different options, and on macOS you will
probably want to use `pbcopy(1)`.

[man-xclip]: https://man.cx/xclip "xclip man page"
[man-xsel]: https://man.cx/xsel "xsel man page"
