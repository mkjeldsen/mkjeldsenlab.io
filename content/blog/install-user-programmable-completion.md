+++
title = "Installing per-user programmable Bash completion optimally"
date = 2020-05-01T20:23:29+02:00
draft = false
tags = [ "bash", "ergonomics" ]
+++

_Programmable completion_ is the assisted generation of commands based on the
current command line context. In an interactive terminal, programmable
completion is the implementation of both the _discoverability_ and
_recognition_ user experience principles; and I personally think it is the only
thing that makes interactive command line applications even remotely tolerable,
let alone pleasant, over extended periods of time. This functionality is so
important that proponents of the Zsh and fish terminal emulators often eagerly
highlight their respective implementations as a point of superiority over the
Bash terminal emulator[^fn-superior].

- [How do I install my own completions?](#how-do-i-install-my-own-completions)
   - [Motivation](#motivation)
   - [The optimal way](#the-optimal-way)
   - [Unsatisfying alternatives](#unsatisfying-alternatives)

## How do I install my own completions?

Programmable completion has to be _programmed_, which means somewhere there is
a _program_ that executes whenever the user requests completion. We call such
a program a <dfn>completion</dfn>.

End-user distributions will usually install completions alongside their owning
applications, when those applications _have_ completions.

What do you do whenever you want to install, on your own, completion for an
application that doesn't already have it?

### Motivation

Reasons you might end up in this situation include

- the application ships with completion but was not installed system-wide, so
  the completion was not hooked into whatever machinery makes completion work;
- [you wrote the completion yourself][url-tldp], and cannot or will not
  upstream it, and at any rate, will not wait for upstreaming;
- the application and completion are shipped separately and the latter is not
  installable system-wide.

If you have root access on the system in question you can, of course, simply
figure out where system-installed completions live and try to mimic system-wide
installation. When executed correctly, this will work; there is no technical
difference between that and any other way. But it's not the _best_ way, and
neither are most of the suggestions you are likely to encounter in the wild.

### The optimal way

For some command `foo`, place its completion in a `completions/` directory in
the _completion user directory_, named as either `foo`, `foo.bash`, or `_foo`
according to your preferences.

1. By default, the completion user directory is
   `$HOME/.local/share/bash-completion/`.
1. If you define `XDG_DATA_HOME`, that path instead becomes
   `$XDG_DATA_HOME/bash-completion/`.
1. If you define `BASH_COMPLETION_USER_DIR`, the path instead becomes
   `$BASH_COMPLETION_USER_DIR/`.

For example, given the command `foo` and its completion `_foo`:

{{< highlight bash >}}
$ ls
foo  _foo
{{< / highlight >}}

the most likely installation procedure is going to be the command

{{< highlight bash >}}
$ cp _foo "$HOME"/.local/share/bash-completion/completions/foo
{{< / highlight >}}

It is probable that you will have to create the directory tree first:

{{< highlight bash >}}
$ mkdir -p "$HOME"/.local/share/bash-completion/completions
{{< / highlight >}}


In the unusual case that you do define one of the environment variables
mentioned above, here is a single, robust one-liner that works for all
3 scenarios:

{{< highlight bash >}}
$ cp _foo "${BASH_COMPLETION_USER_DIR:-${XDG_DATA_HOME:-$HOME/.local/share}/bash-completion}"/completions/foo
{{< / highlight >}}

#### Prerequisite: bash-completion package

In Bash, completions usually either accompany the owning application or live in
the [bash-completion][url-bash-completion] community project and end up
installed system-wide. Some end-user distributions come with the
bash-completion package already installed; other distributions consider the
package too large and technically non-critical to warrant eager installation,
but nearly every distribution, and certainly every mainstream distribution,
offers the package in recognition of its utility.

If you don't have the bash-completion package installed and don't have
privileges to install it, there are good news: it doesn't matter which
alternative installation method you choose as they're all equally sub-optimal.
If you're merely _unwilling_ to install the package then you're also astute
enough to realize that this article wasn't written for you and resourceful
enough to work around it.

#### Advantages of this appraoch

A completion installed in this manner is lazy-loaded on demand whenever the
user requests completion of the owning application. That is, if you type `foo`
and hit the <kbd>Tab</kbd> key, bash-completion will search its various known
locations---including the completion user directory---for `foo`'s completion
and load it if found.

> Loading is implemented in bash-completion's `__load_completion` Bash
function, which you can inspect with the `type` builtin command.

While lazy-loading may not sound like a big deal, every _automatic_ alternative
necessarily executes when Bash starts in interactive mode, which means that the
startup time of every interactive shell---and therefore every terminal
emulator---degrades. Here is a very easy way to demonstrate the long-term
effect of that cost:

{{< highlight bash >}}
$ echo 'sleep 2' >> ~/.bash_completion && time bash -i -c exit
exit

real    0m2,024s
user    0m0,022s
sys     0m0,002s
{{< / highlight >}}

Most completions load in single-digit milliseconds, but since you're installing
per-user completions you're moving outside of your distribution's managed
packages, wherefore all bets are off. Some truly bad actors out there take
dozens of milliseconds to load. The worst ones I know of are Kubernetes'
`kubectl` tool and OpenShift's `oc` tool:

{{< highlight bash >}}
$ time source ~/.local/share/bash-completion/completions/kubectl

real    0m0,027s
user    0m0,027s
sys     0m0,000s
$ time source ~/.local/share/bash-completion/completions/oc

real    0m0,046s
user    0m0,044s
sys     0m0,000s
{{< / highlight >}}

If these are not your primary tools in _every single shell_, that's a huge cost
to pay over and over. Why do they take so long to load? Partly, of course,
those 2 tools both have an enormous surface area and therefore require a lot of
code for satisfactory completion. There is a more insidious reason also at
play, however: both tools leverage a library to generate their completions and
that library generates some _supremely_ disrespectful code. Here is
a bird's-eye comparison to Git:

{{< highlight bash >}}
$ wc < ~/.local/share/bash-completion/completions/kubectl
  7814   8504 243429
$ wc < ~/.local/share/bash-completion/completions/oc
 17168  17077 499148
$ wc < /usr/share/bash-completion/completions/git
 3381  8848 73048
$ time source /usr/share/bash-completion/completions/git

real    0m0,015s
user    0m0,009s
sys     0m0,005s
{{< / highlight >}}

It is possible that `kubectl`'s surface area is truly [more than twice as
large][url-relative] as Git's but it seems improbable to me.

Worse, [that library][url-cobra] is _very_ popular in the Go ecosystem.

Additionally, completions remain isolated from each other and therefore are
a lot more manageable; it is easier to see if a command has user-installed
completion, for purposes of adjusting or uninstalling.

### Unsatisfying alternatives

There are so many other ways you could install per-user completions. Why avoid
those?

Fundamentally, every installation method is functionally identical. Every
completion is just a Bash script sourced by the current shell. Bash doesn't
care who or what sources it, that question is purely about ergonomic
trade-offs.

#### `$HOME/.bashrc`

The advised installation procedure I most frequently encounter in the wild is
to manually source completion from the `$HOME/.bashrc` file. The following
3 commands, which are all functionally equivalent, demonstrate that:

{{< highlight bash >}}
$ pandoc --bash-completion >> "$HOME"/.bashrc
$ echo 'eval "$(pandoc --bash-completion)"' >> "$HOME"/.bashrc
$ echo 'source <(pandoc --bash-completion)' >> "$HOME"/.bashrc
{{< / highlight >}}

Some non-system installers will even unhelpfully inject the relevant code for
you.

You don't want to use the first command because you'll never be able to salvage
your `$HOME/.bashrc` file again. You don't want to use the other 2 for the reasons
I covered above. Instead, concretely, you should use a command like

{{< highlight bash >}}
$ pandoc --bash-completion > "$HOME"/.local/share/bash-completion/completions/pandoc
{{< / highlight >}}

It is no surprise the `$HOME/.bashrc` approach is so widespread. It is
trivially the worse-is-better solution: it can be immediately verified as
working, the downsides are unnoticable in isolation, and the author needs not
take into account that a particular user may not have bash-completion
installed.

#### `$HOME/.bash_completion`

If the `$HOME/.bash_completion` file exists, the bash-completion package will
assume it is a single Bash script that initializes arbitrary completion and
exeute it on login. You can copy any other completion in there, or `source` it
from elsewhere, and it will automatically be available. The only difference
between this approach and `$HOME/.bashrc` lies in what system is responsible
for executing the file; _only_ the bash-completion package will execute this
file.

Despite depending on bash-completion, unlike the completion user directory
method described in [_The optimal way_](#the-optimal-way) this file is executed
eagerly, and so will eventually grow large enough to slow down terminal
start-up. Long before that happens, the file will also have become
unmanageable.

Whether or not this file exists, bash-completion will still load completions
from the completion user directory.

#### System-wide installation

As I suggested earlier, you could always install the completion system-wide
instead of for only your user. If you maintain a multi-user system and
providing this completion to other users is worthwhile, this approach is
genuinely worth considering. However, in that case, you really ought to look
into creating a full-fledged distribution package instead, so you can leverage
the system package manager.

[^fn-superior]: No comment to the question of superiority.
[man-fish]: https://man.cx/fish "fish shell man page"
[man-zsh]: https://man.cx/zsh "Z shell man page"
[url-bash-completion]: https://github.com/scop/bash-completion "bash-completion home page"
[url-cobra]: https://github.com/spf13/cobra "Cobra home page"
[url-tldp]: https://tldp.org/LDP/abs/html/tabexpansion.html "TLDP: An Introduction to Programmable Completion"
[url-relative]: {{< relref "communicating-relative-change" >}} "Communicating relative change"
