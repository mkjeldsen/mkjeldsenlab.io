+++
title = "Reboot directly into boot option"
date = 2020-01-26T11:04:15+01:00
draft = false
tags = [ "bash", "ergonomics" ]
+++

Privileged users can reboot directly into another boot option. This makes for
a convenient way to reboot into

- a secondary operating system when dual-booting;
- an alternative boot configuration; or
- another boot device.

Here are two ways:

- [With UEFI](#with-uefi)
- [With GRUB](#with-grub)

## With UEFI

An UEFI system can boot directly into an alternative boot loader. This is
particularly useful for dual-booting: you need not first boot into the primary
boot loader only to instruct it to reboot into a completely different boot
loader.

[`efibootmgr(8)`][man-efibootmgr] executed with no arguments lists the current
configuration, including the _active_ boot options. The `--bootnext` option
takes as its argument the number of the boot option to boot into next time the
system starts:

{{< highlight "bash" >}}
$ efibootmgr
BootCurrent: 0001
Timeout: 1 seconds
BootOrder: 0001,0000,0002,0003,0004
Boot0000* Windows Boot Manager
Boot0001* ubuntu
Boot0002* UEFI:CD/DVD Drive
Boot0003* UEFI:Removable Device
Boot0004* UEFI:Network Device
$ sudo efibootmgr --bootnext 0000
$ reboot
{{< / highlight >}}

If you're lucky the boot options have parse-friendly _labels_[^fn-parse], in
which case the desired boot option number can be extracted programmatically;
helpful in a few vanishingly rare situations where numbers can be reassigned,
such as in replacing most of your hardware.

In any case, you can make a wrapper to reboot directly into a frequently used
boot option, such as this `winboot`:

{{< highlight "bash" >}}
#!/bin/sh

set -o errexit

winpartition="$(efibootmgr | grep Windows | cut -c 5-8)"

sudo efibootmgr --bootnext $winpartition
reboot
{{< / highlight >}}

You can further disable the `sudo` password prompt with

{{< highlight "bash" >}}
$ ( umask 226 ;
    printf '%s ALL = NOPASSWD: /bin/efibootmgr --bootnext [[\:digit\:]]*\n' \
    "$(whoami)" |
    sudo tee /etc/sudoers.d/winboot )
{{< / highlight >}}

## With GRUB

[`grub-reboot(8)`][man-grub-reboot] can pre-select a different menu entry in
GRUB's regular boot menu, such that it boots into an alternative option when
supplied no user input. Because this leverages the GRUB boot loader, naturally
you have to boot into that boot loader and wait for it to do as instructed.

`grub-reboot` reads---and completes!---boot options from `/boot/grub/grub.cfg`
by default. Each `menuentry` in that file defines an option; the first value
after `menuentry` is the entry name that must be passed to `grub-reboot`:

{{< highlight "bash" >}}
$ grep Windows /boot/grub/grub.cfg | cut -d\' -f 2
Windows Boot Manager (on /dev/sdb2)
$ sudo grub-reboot 'Windows Boot Manager (on /dev/sdb2)'
$ reboot
{{< / highlight >}}

[^fn-parse]: Changing a label involves recreating the option.
[man-efibootmgr]: https://man.cx/efibootmgr "efibootmgr man page"
[man-grub-reboot]: https://man.cx/grub-reboot "grub-reboot man page"
