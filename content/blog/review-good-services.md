+++
title = "Review: Good Services"
date = 2021-07-11T20:32:53+02:00
draft = false
ISBN13 = "9789063695439"
+++

_Good Services_ is a decent draft.

_Good Services_, ISBN-13 9789063695439, is a cheap book, easy to read, and
largely comprehensible. In it, former Design Director of the UK Government Lou
Downe attempts to distil [service design][url-service-design] into 15 tangible
principles for designing a "good service". Note that this is the historical
mass noun meaning of [_service_][url-define-service]:

> The action of helping or doing work for someone.

Not the more recent cultural appropriation perpetrated by the software
industry, whose meaning is closer to that of the word "thing".

_Good Services_' principles are [readily available online,
gratis][url-book-site], in an immediately digestible and applicable manner. In
fact, the only substantial material present in the book and absent from the
book's home page is the author's unique backdrop to and motivation for each
principle. Readers willing to take Downe's advice at face value can skip the
book altogether and just reference the online index. This material earns
a solid, strong recommendation.

As far as the specific principles advocated by _Good Services_ go,
I overwhelmingly agree with them. Not all of them apply all the time but many
apply much of the time---and they tend to be things that are easy to sweep
under the rug when it's too late but also easy to get right from the outset.
I personally believe that "_enabling each user to complete the outcome they set
out to do_" is more important than "_being easy to find_" but that's splitting
hairs and the book actually addresses that.

Sadly, the book wants for a critical editor. Every chapter features jarring
grammatical mistakes. A few chapters leave large gaps between their principles
and supposed motivation, almost as unwelcome and ironic exercises left to the
reader. Later chapters rely increasingly and excessively on niche perspectives
of particular personal significance to the author, which, although completely
legitimate, are simply needlessly difficult for a large segment of the audience
to relate to---and it is hard to imagine that Downe did not encounter
difficulties more relatable to the masses to use as examples. In the end, _Good
Services_ reads like a series of personal blog posts that started with one
direction and veered off into another one with a political agenda. I'd like to
be able to support Downe's work but the book is just too sloppy to earn
a buy-recommendation.

[url-book-site]: https://good.services/ "Book home page"
[url-define-service]: https://www.lexico.com/definition/service "Definition of "service""
[url-service-design]: https://en.wikipedia.org/wiki/Service_design "Wikipedia: Service design"
