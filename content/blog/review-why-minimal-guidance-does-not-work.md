---
title: "Review: Why Minimal Guidance During Instruction Does Not Work"
date: 2021-12-13T19:08:28+01:00
draft: false
---

_Why Minimal Guidance During Instruction Does Not Work: An Analysis of the
Failure of Constructivist, Discovery, Problem-Based, Experiential, and
Inquiry-Based Teaching_[^fn-cite] \("the article") is a survey article
summarizing how, for more than half a century, empirical science has
consistently proven "minimally guided instruction" to be an ineffective method
for teaching novice learners due to the human cognitive architecture, yet,
strangely, scientists keep reinventing minimally guided practices and
practitioners keep using them.

I read this article because it was recommended in _The Programmer's Brain_,
which I have read and would recommend but have not reviewed.

## Summary

The article discusses two approaches to teaching: "guided" and "minimally
guided" (or even unguided) instruction, in reference to the degree of guidance
provided by an instructor. With minimally guided instruction, little to no
guidance is provided: the learner is presented a problem, then left to solve it
without meaningful supervisory help. In contrast, with guided instruction the
instructor proactively assists and directs the learner throughout the problem.

Since 1961, minimally guided instruction practices have been continually
reinvented under several different names, including "constructivist",
"discovery", "problem-based", and "experiential" learning. Proponents of
minimally guided instruction argue that learning is most effective when the
learner constructs their own solutions to "authentic" problems, and that
proactive instruction interferes with their ability to construct knowledge. In
effect, "too much guidance" teaches the learner to recite answers but not to
solve problems.

The studies on which minimally guided instruction practices are founded have
been attempted replicated often, never successfully, always with the conclusion
that minimally guided instruction does not compare favourably to guided
instruction, occasionally with the observation that minimally guided
instruction leads to adverse long-term effects in the form of misconceptions.

It is unlikely that any practice that fails to consider the human cognitive
architecture will be effective, let alone more effective than a practice that
builds on said architecture. This regards the relationship between the
structures of long-term memory and working memory especially. Minimally guided
instruction appears to entirely ignore the human cognitive architecture.

_Expertise_ is a large amount of information we store in and can retrieve from
our long-term memory. Experts employ schema-based pattern recognition when
problem solving rather than specific methods, and they excel at knowledge
organization and schema acquisition. To learn means to permanently alter
long-term memory, so instruction should optimize for effectively and
efficiently altering long-term memory.

We are not conscious of information stored in long-term memory. Instead,
conscious processing occurs in working memory. When processing new information,
the working memory's capacity is limited to about 4 elements and information
expires after approximately 30 seconds without rehearsal. However, working
memory can recover information from long-term memory seemingly indefinitely.
Problem solving taxes the working memory yet is central to at least one
minimally guided instruction practice.

Studies have found that medical students subjected to minimally guided
instruction, when compared to conventional instruction, ordered significantly
more unnecessary tests at higher cost per patient; were required to spend more
time in clinical settings; scored lower on basic science exams; and studied for
more hours per day. They have also found that such students are less capable of
abstracting knowledge, produce more errors in reasoning, and retain this
backward-directed reasoning pattern in perpetuity---that is, they acquire
counterproductive schemata they cannot get rid of.

## Discussion

_Experiental learning_ is the academic name for _learning by doing_. All the
other minimally guided instruction practices are different names for
substantially the same thing.

The notion that "too much guidance" is detrimental is tautological, by
definition of "too much". It is a non-argument, as well as a _fallacy of the
undistributed middle_[^fn-fallacy].

The degree of guidance is a gradient so the line between minimally guided and
guided learning is blurry, not to say undefined. The two sides seem to be
defined more by an attitude towards instruction than by a measurement: if the
instructor proactively engages in instruction it is guided teaching, otherwise
it is minimally guided. One proponent of "problem based learning" suggested
that the practice may be improved with direct instruction provided on an
as-needed basis---which would effectively turn problem based learning from
being a minimally guided practice into a guided practice.

Minimally guided instruction is not unworkable, but relative to guided
instruction it is...

* ... ineffective: learned knowledge is less complete and cohesive, and
  accompanied with more errors during practice.
* ... inefficient: absent guidance, the learner risks personally retreading
  a vast surface area of society's collective knowledge.

Intuition supports the notion that guided instruction is more effective and
efficient than minimally guided instruction: if that were not true, much of the
infrastructure for organized teaching would be better replaced by no teaching.

Ironically, the article title has a word count of 5&times; the working memory's
limit, without being obviously better than merely "Minimal Guidance Does Not
Work".

### Extrapolated to onboarding

Do not give a learner a task to build something and leave them alone. Not even
a "small" thing. Do not tell them to "look around". Do not wait for or expect
them to ask questions, let alone effective questions. They will get there, but
they will take much longer to get there than is necessary and they will have
a confused picture of how they got there. This will make them personally less
skilled than otherwise, and at length will impact their ability to teach
effectively.

Instead, pair-program, with the teacher guiding and explaining as the learner
writes. The learner will experience fewer false starts and dead-ends, will have
a much clearer picture of coupling, and will be much more comfortable asking
questions. The learner's schemata will be more cohesive and less polluted by
misconceptions.

Concentrate on a few moving parts at a time and avoid tangents. If you go down
a tangent, make that clear. This avoids overloading working memory so the
learner can more reliably form long-term memories. Reiterate to rehearse.

[^fn-cite]: Paul A. Kirschner, John Sweller & Richard E. Clark (2006) Why Minimal Guidance During Instruction Does Not Work: An Analysis of the Failure of Constructivist, Discovery, Problem-Based, Experiential, and Inquiry-Based Teaching, Educational Psychologist, 41:2, 75-86, DOI: <https://doi.org/10.1207/s15326985ep4102_1>

[^fn-fallacy]: "1) All cases of too much guidance are ineffective. 2) Some cases of guidance are cases of too much guidance. 3) Therefore, all cases of guidance are ineffective."
