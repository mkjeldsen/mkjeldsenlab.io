+++
title = "About"
date = 2018-05-30T20:29:33+02:00
draft = false
+++

Hello! I'm a software developer. Sometimes I write about that
here -- for myself and anyone that wants to read.

Nice to meet you!

## Copyright

Unless otherwise noted, content on this site is available under an [Attribution
4.0 International][cc] license. [All code][repo] is additionally available
under either the [Apache-2.0][apache-2.0] license or the [MIT][mit] license,
subject to preference.

### Third-party

The site is built with [Hugo][hugo], released under the Apache-2.0 license. It
uses a theme based on [_Cocoa_][cocoa], released under the MIT license.

## Privacy

I don't actively track activity in any way -- that is, I use no analytics,
commenting integrations, or the like. I do use some third-part services; here
are their privacy policies:

- [GitLab Pages][gitlab-privacy]
- [Google Fonts][google-fonts]
- [jsDelivr CDN][jsdelivr-privacy]

[apache-2.0]: https://spdx.org/licenses/Apache-2.0.html "Apache-2.0 license at SPDX"
[cc]: https://creativecommons.org/licenses/by/4.0/legalcode "CC BY 4.0 license"
[cocoa]: https://themes.gohugo.io/cocoa/ "Cocoa Hugo theme"
[gitlab-privacy]: https://about.gitlab.com/privacy/ "GitLab Privacy Policy"
[google-fonts]: https://developers.google.com/fonts/faq#what_does_using_the_google_fonts_api_mean_for_the_privacy_of_my_users "Google Fonts FAQ: privacy"
[hugo]: https://gohugo.io/ "Official Hugo Web site"
[jsdelivr-privacy]: https://www.jsdelivr.com/privacy-policy-jsdelivr-net
[mit]: https://spdx.org/licenses/MIT.html "MIT license at SPDX"
[repo]: https://gitlab.com/mkjeldsen/mkjeldsen.gitlab.io "Code repository"
